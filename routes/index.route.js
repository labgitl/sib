const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');

const User = require('../models/user.model');

router.get('/', async (req, res) => {
    /*var user = new User(
        {
            name: 'demo',
            user_name: 'demo',
            email: 'demo@hotmail.com',
            password: 'demo'
        }
    );
    await user.save(function (err) {
        if (err) {
            return next(err);
        }
        //res.send('User Created successfully')
    })*/
    var query = {user_name: 'demo'},
    update = { name: 'demo' },
    options = { upsert: false };

// Find the document
const users = await User.findOneAndUpdate(query, update, options, function(error, result) {
    if (!error) {
        // If the document doesn't exist
        if (!result) {
            // Create it
            result = new User(
                {
                    name: 'demo',
                    user_name: 'demo',
                    email: 'demo@hotmail.com',
                    password: 'demo'
                }
            );
        }
        // Save the document
        result.save(function(error) {
            if (!error) {
                // Do something with the document
            } else {
                throw error;
            }
        });
    }

    // do something with the document
});
    //const users = await User.find();
    console.log(users);
    res.render('index', {
        users // users: users
    });
});

router.get('/login', async (req, res) => {
    //const{error} = loginValidation(req.body);
    //if (error) return res.status(400).send(error.details[0].message);
    console.log(req.query)
    const emailExist = await User.findOne({user_name: req.query.user_name, password: req.query.password});
    if (!emailExist) return res.status(400).redirect('/');//'Email or password is wrong'
    //password is correct
    //const validPass = await bcrypt.compare(req.body.password, user.password);

    const token = jwt.sign({_id:emailExist._id}, process.env.TOKEN_SECRET);//'iufbasudfubnsdfuf'
    res.header('auth-token', token)//.send(token);
    res.set('auth-token', token);

    console.log(token);

    //res.send('login innnnnnnnnnnnnnnnnnnnn '+token)
    res.redirect('/users');
});

module.exports = router;