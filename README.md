# SiB test

No solo hice una api en nodejs, **tambien hice una vista para el uso de la misma**

**La base de datos en mongodb se genera al momento de ejecutar la aplicacion**

## Instrucciones

Ingresar a localhost haciendo uso del puerto especificado en el proyecto e ingresar "demo" como usuario y contraseña

## Descripcion de proyecto

Para "crear usuario" he generado "routes/user.route.js" cual captura los datos enviados 
desde el formulario en la vista "views/indexu.js" para nuevos usuarios y desde "views/edit.js" 
para ser editados. Datos que son enviados a "controllers/user.controller.js" haciendo uso 
del modelo "user" en "models/user.model.js". La misma ruta, controlador y modelo son usados 
por el formulario buscar en "views/buscar.ejs" y para eliminar usuarios haciendo uso de 
los botones en la tabla a la derecha del formulario "usuarios"

Para el login se hace uso del formulario en "views/index.ejs" y de "routes/index.route.js" al igual que 
el controlador y modelo "usuario". Por defecto cada vez que se ejecuta la aplicacion se genera un usuario "demo"

El uso de tokens se implementa en "verifytoken.js" para ser llamado en "routes/user.route.js"

## Puntos realizados

Se genero un repositorio en gitlab

Se genero un archivo readme con la informacion del proyecto y de despliegue

Se genero api y vista para generar nuevos usuarios, buscar usuarios por user_name, modificar usuario y eliminar 
usuario

Se genero vistas para login y logout

Se implemento dotenv para variables de configuracion y JWT

La api se puede proteger con tan solo cambiar en "routes/user.route.js"
    
    router.get('/', user_controller.test); 

por 
    
    router.get('/', verify, user_controller.test);
    
Lo mismo aplica en todas las rutas